#ifndef MSPTOOLS_H
#define MSPTOOLS_H

#include "array.h"
#include "array2d.h"
#include "carray2d.h"
#include "ndarray.h"
#include "sparse.h"
#include "sllist.h"

#define MSP_VER 1 /* MSPtools Version 1.0.1 */
#define MSP_SUBVER 0
#define MSP_SUBSUB 1
#define MSP_DATE "Nov 3, 2024" /* Release date */
#define MSP_COPYRIGHT "Copyright (c) Martin S. Andersen, 2021-"

#endif
